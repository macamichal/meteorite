// Generated code from Butter Knife. Do not modify!
package com.android.maca.meteoritelandings.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ListActivity$$ViewInjector<T extends com.android.maca.meteoritelandings.activity.ListActivity> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131427432, "field 'mCount'");
    target.mCount = finder.castView(view, 2131427432, "field 'mCount'");
  }

  @Override public void reset(T target) {
    target.mCount = null;
  }
}
