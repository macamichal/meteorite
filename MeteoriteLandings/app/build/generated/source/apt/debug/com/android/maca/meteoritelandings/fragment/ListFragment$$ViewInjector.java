// Generated code from Butter Knife. Do not modify!
package com.android.maca.meteoritelandings.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class ListFragment$$ViewInjector<T extends com.android.maca.meteoritelandings.fragment.ListFragment> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131427430, "field 'mInfo'");
    target.mInfo = finder.castView(view, 2131427430, "field 'mInfo'");
    view = finder.findRequiredView(source, 2131427431, "field 'mRecyclerView'");
    target.mRecyclerView = finder.castView(view, 2131427431, "field 'mRecyclerView'");
  }

  @Override public void reset(T target) {
    target.mInfo = null;
    target.mRecyclerView = null;
  }
}
