package com.android.maca.meteoritelandings.retrofit.service;

import com.android.maca.meteoritelandings.pojo.Meteorite;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by michal on 14.11.17.
 */

public interface MeteoriteService {

    /**
     * Load meteorites data.
     *
     * @param from is date from load
     * @param order is column order by
     * @return
     */
    @GET("y77d-th95.json")
    Observable<List<Meteorite>> getAll(@Query("$where") String from, @Query("$order") String order);
}
