package com.android.maca.meteoritelandings.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.maca.meteoritelandings.App;
import com.android.maca.meteoritelandings.R;
import com.android.maca.meteoritelandings.adapter.MeteoriteAdapter;
import com.android.maca.meteoritelandings.pojo.Meteorite;
import com.android.maca.meteoritelandings.retrofit.ApiUtils;
import com.android.maca.meteoritelandings.retrofit.service.MeteoriteService;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by michal on 14.11.17.
 */

public class ListFragment extends Fragment {

    private static final String TAG = ListFragment.class.getSimpleName();
    private static String SAVED_INSTANCE_ITEMS = "items";

    private Callbacks mCallbacks = sDummyCallbacks;


    private MeteoriteAdapter mAdapter;
    private MeteoriteService mService;

    @InjectView(R.id.tv_info)
    TextView mInfo;

    @InjectView(R.id.rv_items)
    RecyclerView mRecyclerView;

    private List<Meteorite> mList = null;

    public interface Callbacks {
        public void onItemSelected(Meteorite meteorite);
    }

    public static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(Meteorite meteorite) {
        }
    };


    public ListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.inject(this, root);

        initialize();

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!App.isNetworkAvailable()) {
            mInfo.setText(getString(R.string.no_internet));
            return;
        }

        if (savedInstanceState != null) {
            mList = savedInstanceState.getParcelableArrayList(SAVED_INSTANCE_ITEMS);
            Log.d(TAG, "load from INSTANCE");
            showList();
        } else {
            Log.d(TAG, "NEW load");
            loadList();
            showList();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(context instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = sDummyCallbacks;
    }

    private void initialize() {
        mService = ApiUtils.getMeteoriteService();
        mAdapter = new MeteoriteAdapter(new ArrayList<Meteorite>(0), new MeteoriteAdapter.PostItemListener() {

            @Override
            public void onPostClick(Meteorite item) {
                Log.d(TAG, item.toString());
                mCallbacks.onItemSelected(item);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
    }


    public void loadList() {
        Log.d(TAG, "load list");

        mService.getAll("year >= '2011-01-01T00:00:00.000'", "mass DESC").subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Meteorite>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "finish download");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.toString() + e.getMessage());
                    }

                    @Override
                    public void onNext(List<Meteorite> response) {
                        Log.d(TAG, response.toString());
                        mList = response;
                        showList();
                    }
                });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mList != null) {
            outState.putParcelableArrayList(SAVED_INSTANCE_ITEMS, (ArrayList<? extends Parcelable>) mList);
        }
    }

    /**
     * Show list data.
     */
    private void showList() {
        mAdapter.updateList(mList);
        mInfo.setText(getString(R.string.total_count, mAdapter.getItemCount()));
    }
}
