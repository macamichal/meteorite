package com.android.maca.meteoritelandings.retrofit;

import com.android.maca.meteoritelandings.retrofit.service.MeteoriteService;

/**
 * Created by michal on 14.11.17.
 */

public class ApiUtils {

    public static final String BASE_URL = "https://data.nasa.gov/resource/";

    /**
     * Get instance of meteorite service.
     *
     * @return
     */
    public static MeteoriteService getMeteoriteService() {
        return RetrofitClient.getClient(BASE_URL).create(MeteoriteService.class);
    }
}