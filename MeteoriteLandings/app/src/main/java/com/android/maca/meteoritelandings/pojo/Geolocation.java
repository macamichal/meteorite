package com.android.maca.meteoritelandings.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 14.11.17.
 */

public class Geolocation implements Parcelable {


    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("coordinates")
    @Expose
    private List<Double> coordinates = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Geolocation{" +
                "type='" + type + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeList(this.coordinates);
    }

    public Geolocation() {
    }

    protected Geolocation(Parcel in) {
        this.type = in.readString();
        this.coordinates = new ArrayList<Double>();
        in.readList(this.coordinates, Double.class.getClassLoader());
    }

    public static final Parcelable.Creator<Geolocation> CREATOR = new Parcelable.Creator<Geolocation>() {
        @Override
        public Geolocation createFromParcel(Parcel source) {
            return new Geolocation(source);
        }

        @Override
        public Geolocation[] newArray(int size) {
            return new Geolocation[size];
        }
    };
}
