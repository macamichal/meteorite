package com.android.maca.meteoritelandings.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michal on 14.11.17.
 */

public class Meteorite implements Parcelable {

    @SerializedName("fall")
    @Expose
    private String fall;
    @SerializedName("geolocation")
    @Expose
    private Geolocation geolocation;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("mass")
    @Expose
    private String mass;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nametype")
    @Expose
    private String nametype;
    @SerializedName("recclass")
    @Expose
    private String recclass;
    @SerializedName("reclat")
    @Expose
    private String reclat;
    @SerializedName("reclong")
    @Expose
    private String reclong;
    @SerializedName("year")
    @Expose
    private String year;

    public String getFall() {
        return fall;
    }

    public void setFall(String fall) {
        this.fall = fall;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNametype() {
        return nametype;
    }

    public void setNametype(String nametype) {
        this.nametype = nametype;
    }

    public String getRecclass() {
        return recclass;
    }

    public void setRecclass(String recclass) {
        this.recclass = recclass;
    }

    public String getReclat() {
        return reclat;
    }

    public void setReclat(String reclat) {
        this.reclat = reclat;
    }

    public String getReclong() {
        return reclong;
    }

    public void setReclong(String reclong) {
        this.reclong = reclong;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Meteorite{" +
                "fall='" + fall + '\'' +
                ", geolocation=" + geolocation +
                ", id='" + id + '\'' +
                ", mass='" + mass + '\'' +
                ", name='" + name + '\'' +
                ", nametype='" + nametype + '\'' +
                ", recclass='" + recclass + '\'' +
                ", reclat='" + reclat + '\'' +
                ", reclong='" + reclong + '\'' +
                ", year='" + year + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fall);
        dest.writeParcelable(this.geolocation, flags);
        dest.writeString(this.id);
        dest.writeString(this.mass);
        dest.writeString(this.name);
        dest.writeString(this.nametype);
        dest.writeString(this.recclass);
        dest.writeString(this.reclat);
        dest.writeString(this.reclong);
        dest.writeString(this.year);
    }

    public Meteorite() {
    }

    protected Meteorite(Parcel in) {
        this.fall = in.readString();
        this.geolocation = in.readParcelable(Geolocation.class.getClassLoader());
        this.id = in.readString();
        this.mass = in.readString();
        this.name = in.readString();
        this.nametype = in.readString();
        this.recclass = in.readString();
        this.reclat = in.readString();
        this.reclong = in.readString();
        this.year = in.readString();
    }

    public static final Parcelable.Creator<Meteorite> CREATOR = new Parcelable.Creator<Meteorite>() {
        @Override
        public Meteorite createFromParcel(Parcel source) {
            return new Meteorite(source);
        }

        @Override
        public Meteorite[] newArray(int size) {
            return new Meteorite[size];
        }
    };
}
