package com.android.maca.meteoritelandings.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import com.android.maca.meteoritelandings.R;
import com.android.maca.meteoritelandings.fragment.DetailFragment;
import com.android.maca.meteoritelandings.fragment.ListFragment;
import com.android.maca.meteoritelandings.pojo.Meteorite;

public class ListActivity extends FragmentActivity implements ListFragment.Callbacks {
    private boolean mTwoPane;

    private static final String TAG = ListActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        if (findViewById(R.id.circle_detail_container) != null) {
            mTwoPane = true;
        }

    }

    @Override
    public void onItemSelected(Meteorite meteorite) {
        Log.d(TAG, meteorite.toString());
        //one ore two columns
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(DetailFragment.ARG_ITEM, meteorite);
            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.circle_detail_container, fragment).commit();

        } else {
            Intent detailIntent = new Intent(this, DetailActivity.class);
            detailIntent.putExtra(DetailFragment.ARG_ITEM, meteorite);
            startActivity(detailIntent);
        }
    }
}
