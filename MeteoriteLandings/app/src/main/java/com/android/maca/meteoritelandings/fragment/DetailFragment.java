package com.android.maca.meteoritelandings.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.maca.meteoritelandings.R;
import com.android.maca.meteoritelandings.pojo.Meteorite;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by michal on 14.11.17.
 */

public class DetailFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = DetailFragment.class.getSimpleName();
    public static final String ARG_ITEM = "arg_item";
    private Meteorite mItem = null;
    private static View mView;

    public DetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM)) {
            mItem = getArguments().getParcelable(ARG_ITEM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_detail, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }



        ButterKnife.inject(this, mView);

        if (mItem != null) {
            Log.d(TAG, mItem.toString());

            //initialize map
            MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        return mView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //create mark
        googleMap.clear();
        LatLng item = new LatLng(mItem.getGeolocation().getCoordinates().get(0), mItem.getGeolocation().getCoordinates().get(1));
        googleMap.addMarker(new MarkerOptions().position(item)
                .title(mItem.getName()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(item, 3));
    }
}
