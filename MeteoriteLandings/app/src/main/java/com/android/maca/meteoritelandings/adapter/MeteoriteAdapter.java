package com.android.maca.meteoritelandings.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.maca.meteoritelandings.R;
import com.android.maca.meteoritelandings.pojo.Meteorite;

import java.util.List;

/**
 * Created by michal on 14.11.17.
 */

public class MeteoriteAdapter extends RecyclerView.Adapter<MeteoriteAdapter.ViewHolder> {

    private List<Meteorite> mItems;
    private PostItemListener mItemListener;


    public MeteoriteAdapter(List<Meteorite> posts, PostItemListener itemListener) {
        mItems = posts;
        mItemListener = itemListener;
    }

    @Override
    public MeteoriteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View postView = inflater.inflate(R.layout.item_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(postView, this.mItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MeteoriteAdapter.ViewHolder holder, int position) {
        Meteorite item = mItems.get(position);
        //set values
        holder.nameTv.setText(item.getName());
        holder.massTv.setText(item.getMass());
        holder.coordinatesTv.setText(item.getGeolocation().getCoordinates().toString());
    }

    @Override
    public int getItemCount() {
        if(mItems == null) {
            return 0;
        }
        return mItems.size();
    }

    public void updateList(List<Meteorite> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    private Meteorite getItem(int adapterPosition) {
        return mItems.get(adapterPosition);
    }

    public interface PostItemListener {
        void onPostClick(Meteorite item);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //define items
        public TextView nameTv;
        public TextView massTv;
        public TextView coordinatesTv;
        PostItemListener mItemListener;

        public ViewHolder(View itemView, PostItemListener postItemListener) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.tv_name);
            massTv = (TextView) itemView.findViewById(R.id.tv_mass);
            coordinatesTv = (TextView) itemView.findViewById(R.id.tv_coordinates);

            this.mItemListener = postItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Meteorite item = getItem(getAdapterPosition());
            this.mItemListener.onPostClick(item);

            notifyDataSetChanged();
        }
    }
}