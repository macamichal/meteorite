package com.android.maca.meteoritelandings.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.android.maca.meteoritelandings.R;
import com.android.maca.meteoritelandings.fragment.DetailFragment;

/**
 * Created by michal on 14.11.17.
 */

public class DetailActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (savedInstanceState == null) {
            //show fragment detail
            Bundle arguments = new Bundle();
            arguments.putParcelable(DetailFragment.ARG_ITEM, getIntent()
                    .getParcelableExtra(DetailFragment.ARG_ITEM));
            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.circle_detail_container, fragment).commit();
        }
    }
}
